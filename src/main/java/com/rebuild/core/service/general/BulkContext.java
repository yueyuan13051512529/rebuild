/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.general;

import cn.devezhao.bizz.privileges.Permission;
import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.RebuildException;
import com.rebuild.core.metadata.MetadataHelper;
import org.apache.commons.lang.ArrayUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class BulkContext {

    
    private ID opUser;
    
    private Permission action;
    
    private ID toUser;

    
    private ID[] records;
    
    private ID targetRecord;
    
    private String[] cascades;

    
    
    
    private Map<String, Object> extraParams = new HashMap<>();

    final private Entity mainEntity;

    
    BulkContext(ID opUser, Permission action, ID toUser, String[] cascades, ID[] records, ID recordMain, Map<String, Object> extraParams) {
        this.opUser = opUser;
        this.action = action;
        this.toUser = toUser;
        this.records = records;
        this.targetRecord = recordMain;
        this.cascades = cascades;
        if (extraParams != null) this.extraParams.putAll(extraParams);
        this.mainEntity = detecteMainEntity();
    }

    
    public BulkContext(ID opUser, Permission action, ID toUser, String[] cascades, ID[] records) {
        this(opUser, action, toUser, cascades, records, null, null);
    }

    
    public BulkContext(ID opUser, Permission action, ID[] records, ID targetRecord) {
        this(opUser, action, null, null, records, targetRecord, null);
    }

    
    public BulkContext(ID opUser, Permission action, JSONObject customData) {
        this(opUser, action, null, null, null, null,
                Collections.singletonMap("customData", customData));
    }

    public ID getOpUser() {
        return opUser;
    }

    public Permission getAction() {
        return action;
    }

    public ID getToUser() {
        return toUser;
    }

    public String[] getCascades() {
        return cascades == null ? ArrayUtils.EMPTY_STRING_ARRAY : cascades;
    }

    public ID[] getRecords() {
        return records;
    }

    public ID getTargetRecord() {
        return targetRecord;
    }

    public Map<String, Object> getExtraParams() {
        return extraParams;
    }

    public void addExtraParam(String name, Object value) {
        extraParams.put(name, value);
    }

    public Entity getMainEntity() {
        return mainEntity;
    }

    private Entity detecteMainEntity() {
        if (targetRecord != null) {
            return MetadataHelper.getEntity(targetRecord.getEntityCode());
        } else if (records != null && records.length > 0) {
            return MetadataHelper.getEntity(records[0].getEntityCode());
        } else if (extraParams.containsKey("customData")) {
            JSONObject customData = (JSONObject) extraParams.get("customData");
            return MetadataHelper.getEntity(customData.getString("entity"));
        }
        throw new RebuildException("No operation record");
    }
}
